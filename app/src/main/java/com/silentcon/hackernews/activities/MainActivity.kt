package com.silentcon.hackernews.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import com.silentcon.hackernews.R
import com.silentcon.hackernews.adapters.ItemRecyclerViewAdapter
import com.silentcon.hackernews.databinding.ActivityMainBinding
import com.silentcon.hackernews.services.HackerNews
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : ActivityBase<ActivityMainBinding>(),
        SwipeRefreshLayout.OnRefreshListener {
    override val layout: Int = R.layout.activity_main
    private var adapter: ItemRecyclerViewAdapter = ItemRecyclerViewAdapter(HackerNews.items)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with (activity_main_toolbar as Toolbar) {
            title = getString(R.string.app_name)
            setTitleTextColor(Color.WHITE)
        }

        // Recyclerview
        activity_main_recyclerview.layoutManager = LinearLayoutManager(this)
        activity_main_recyclerview.adapter = adapter
        adapter.onClickSubject.subscribe {v ->
            intent = Intent(this, ItemActivity::class.java)
            intent.putExtra("item", v.item)
            startActivity(intent)
        }

        // Swipe to refresh
        activity_main_swiperefresh.setOnRefreshListener(this)

        // Firebase
        HackerNews.loadTopStories(adapter)

    }

    override fun onRefresh() {
        HackerNews.loadTopStories(adapter,
                {_ -> activity_main_swiperefresh.isRefreshing = false; })
    }


}
