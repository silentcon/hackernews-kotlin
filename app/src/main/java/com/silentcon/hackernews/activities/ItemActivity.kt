package com.silentcon.hackernews.activities

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.Toast
import com.silentcon.hackernews.R
import com.silentcon.hackernews.adapters.CommentRecyclerViewAdapter
import com.silentcon.hackernews.databinding.ActivityItemBinding
import com.silentcon.hackernews.models.Item
import com.silentcon.hackernews.services.HackerNews
import com.silentcon.hackernews.viewmodels.CommentsNavViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_item.*

class ItemActivity : ToolBarActivity<ActivityItemBinding>() {
    override var toolbar: Lazy<Toolbar> = lazy { activity_item_toolbar as Toolbar }
    override val layout: Int = R.layout.activity_item

    /** The item that is displayed */
    var item: Item = Item()
        set(value) {
            field = value
            activity_item_item.item = value
        }
    private var comments = mutableListOf<Item>()
    private var adapter: CommentRecyclerViewAdapter = CommentRecyclerViewAdapter(comments)
    private lateinit var gestureDetector: GestureDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Get item from intent
        item = intent.extras.getParcelable("item")

        // Fetch replies for this item
        activity_item_recyclerview.layoutManager = LinearLayoutManager(this)
        activity_item_recyclerview.adapter = adapter
        loadComments()

        // Swipe left edge to go back
        gestureDetector = GestureDetector(this, object: GestureDetector.SimpleOnGestureListener() {
            private val SWIPE_DISTANCE_THRESHOLD = 100
            private val SWIPE_VELOCITY_THRESHOLD = 100

            override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
                if (e1 != null && e2 != null) {
                    if (e1.x < 100) {
                        val distanceX = e2.x - e1.x
                        val distanceY = e2.y - e1.y
                        if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (distanceX > 0)
                                finish()

                            return true
                        }
                    }
                }
                return false
            }
        })

        // Navigation
        activity_item_nav.binding.viewModel = CommentsNavViewModel(item, comments)
        activity_item_nav.init(activity_item_recyclerview)
    }

    //override fun dispatchTouchEvent(ev: MotionEvent?): Boolean = gestureDetector.onTouchEvent(ev)

    private fun loadComments() {
        HackerNews.getInnerKids(item)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ c ->
                    c.removeAt(0)    // getInnerKids includes parent item (which is not a comment)
                    comments.clear()
                    comments.addAll(c)
                    adapter.notifyDataSetChanged()
                    activity_item_nav.binding.viewModel?.updateLabel()
                    activity_item_swiperefresh.startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL)
                    activity_item_scrollview.scrollBy(0, 100)
                }, Throwable::printStackTrace)
    }
}