package com.silentcon.hackernews.activities

import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.silentcon.hackernews.R
import kotlinx.android.synthetic.main.toolbar_main.*

/**
 * Activity with toolbar
 */
abstract class ToolBarActivity<B: ViewDataBinding> : ActivityBase<B>() {
    abstract var toolbar: Lazy<Toolbar>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar()
    }

    protected fun initToolbar() {
        setSupportActionBar(toolbar.value)
        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        // Back button action
        toolbar.value.setNavigationOnClickListener { _ -> finish() }
    }
}