package com.silentcon.hackernews.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.silentcon.hackernews.R
import com.silentcon.hackernews.databinding.RowItemBinding
import com.silentcon.hackernews.models.Item
import com.silentcon.hackernews.views.ItemView
import io.reactivex.subjects.PublishSubject

class ItemRecyclerViewAdapter(val items: List<Item>)
    : RecyclerView.Adapter<BindingViewHolder<ItemView, RowItemBinding>>() {

    val onClickSubject: PublishSubject<ItemView> = PublishSubject.create<ItemView>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BindingViewHolder<ItemView, RowItemBinding> {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(R.layout.row_item, parent, false) as ItemView
        return BindingViewHolder(view)
    }

    override fun onBindViewHolder(holder: BindingViewHolder<ItemView, RowItemBinding>?, position: Int) {
        holder?.binding?.item = items[position]
        holder?.binding?.executePendingBindings()

        holder?.itemView?.setOnClickListener { v: View? ->
            if (v != null) {
                onClickSubject.onNext(v as ItemView)
            }
        }
    }

    override fun getItemCount(): Int = items.size
}

