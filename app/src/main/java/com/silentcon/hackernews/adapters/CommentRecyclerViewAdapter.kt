package com.silentcon.hackernews.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.silentcon.hackernews.R
import com.silentcon.hackernews.models.Item
import com.silentcon.hackernews.views.CommentView
import org.apache.commons.text.StringEscapeUtils

class CommentRecyclerViewAdapter(val comments: List<Item>)
    : RecyclerView.Adapter<ViewHolder<CommentView>>() {
    override fun onBindViewHolder(holder: ViewHolder<CommentView>?, position: Int) {
        if (holder != null) {
            val comment = comments[position]
            with (holder.view) {
                item = comment
                text = StringEscapeUtils.unescapeHtml4(comment.text)?.replace("<p>", "\n\n")?.trim()
                level = calculateLevel(this)
            }
        }
    }

    fun calculateLevel(view: CommentView): Int {
        var level = 0
        var current: Item? = view.item
        while (current?.parent != null) {
            current = comments.find { item -> item.id == current?.parent }
            if (current == null)
                break

            level++
        }

        return level
    }

    override fun getItemCount(): Int = comments.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder<CommentView> {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.row_comment, parent, false) as CommentView
        return ViewHolder(view)
    }
}