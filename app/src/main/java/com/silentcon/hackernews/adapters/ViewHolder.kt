package com.silentcon.hackernews.adapters

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import com.silentcon.hackernews.BR

class ViewHolder<T : View>(val view: T) : RecyclerView.ViewHolder(view)

class BindingViewHolder<T: View, B: ViewDataBinding>(val view: T) : RecyclerView.ViewHolder(view)  {
    val binding = DataBindingUtil.bind<B>(view)
}