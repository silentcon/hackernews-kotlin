package com.silentcon.hackernews.services

import android.support.v7.widget.RecyclerView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.silentcon.hackernews.adapters.ItemRecyclerViewAdapter
import com.silentcon.hackernews.models.Item
import durdinapps.rxfirebase2.DataSnapshotMapper
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.operators.single.SingleDoOnSubscribe
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

object HackerNews {
    val FIREBASE_URL = "https://hacker-news.firebaseio.com"
    val API_VERSION = "v0"
    val TOP_STORIES_URL = "$API_VERSION/topstories"
    val ITEM_URL = "$API_VERSION/item"
    val database = FirebaseDatabase.getInstance(FIREBASE_URL)
    val topStoriesDatabase = database.getReference(TOP_STORIES_URL)
    val itemDatabase = database.getReference(ITEM_URL)

    var topStories = mutableListOf<Int>()
    var items = mutableListOf<Item>()

    fun getItemChild(i: Int): DatabaseReference = itemDatabase.child(i.toString())

    /**
     * @param adapter        Needed so that adapter can be notified of changes
     * @param onSubscribe    Function for side-effecs
     */
    fun loadTopStories(adapter: RecyclerView.Adapter<*>,
                       onSubscribe: ((t: Disposable) -> Unit) = {}) {
        RxFirebaseDatabase.observeSingleValueEvent(
                HackerNews.topStoriesDatabase,
                DataSnapshotMapper.listOf(Int::class.java))
                .toObservable()
                .flatMapIterable { list ->
                    topStories = list
                    list
                }
                .flatMap { item ->
                    RxFirebaseDatabase.observeSingleValueEvent(
                            HackerNews.getItemChild(item),
                            Item::class.java).toObservable()
                }
                .toList()
                .doOnSubscribe(onSubscribe)
                .subscribe({newItems ->
                    this.items.clear()
                    this.items.addAll(newItems)
                    adapter.notifyDataSetChanged()
                }, Throwable::printStackTrace)
    }

    fun getItem(id: Int) : Observable<Item> {
        return RxFirebaseDatabase.observeSingleValueEvent(
                HackerNews.getItemChild(id),
                Item::class.java).toObservable()
    }

    /**
     * @see https://stackoverflow.com/questions/31246088/how-to-do-recursive-observable-call-in-rxjava
     */
    fun getInnerKids(item: Item) : Observable<Item> {
        if (item.kids.isNotEmpty()) {
            return Observable.merge(
                    Observable.just(item),
                    Observable.fromIterable(item.kids)
                            .concatMap {i: Int -> getItem(i)}
                            .concatMap(this::getInnerKids))
        }

        return Observable.just(item)
    }
}

enum class Sorting {
    DEFAULT, VOTES, NEW
}