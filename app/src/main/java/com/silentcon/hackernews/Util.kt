package com.silentcon.hackernews

import android.view.LayoutInflater
import android.view.ViewGroup

fun ViewGroup.inflateView(layoutId: Int) {
    val inflater = LayoutInflater.from(context)
    inflater.inflate(layoutId, this)
}