package com.silentcon.hackernews.models

import android.content.res.Resources
import android.os.Parcel
import android.os.Parcelable
import com.silentcon.hackernews.R
import com.silentcon.hackernews.services.HackerNews
import com.squareup.moshi.Json
import durdinapps.rxfirebase2.RxFirebaseDatabase
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.HttpUrl
import org.joda.time.DateTime
import org.joda.time.Instant
import org.joda.time.Interval
import org.joda.time.format.DateTimeFormat
import java.util.*
import kotlin.Comparator

class Item(val id: Int = -1) : Parcelable {
    enum class Type {
        @Json(name = "job")
        JOB,
        @Json(name = "story")
        STORY,
        @Json(name = "comment")
        COMMENT,
        @Json(name = "poll")
        POLL,
        @Json(name = "pollopt")
        POLLOPT
    }

    var deleted: Boolean? = false
    var type: String? = null
    var by: String? = null
    var time: Long? = null
    var text: String? = null
    var dead: Boolean? = false
    var parent: Int? = null
    var poll: Int? = null
    var kids: List<Int> = emptyList()
    var url: String? = null
    var score: Int? = null
    var title: String? = null
    var parts: List<Int> = emptyList()
    var descendants: Int? = null

    var domain: String? = null
        get() {
            if (url != null) {
                try {
                    val domain = HttpUrl.parse(url!!)?.topPrivateDomain()
                    return domain
                } catch (e: Exception) { }
            }

            return null
        }
        private set

    constructor(parcel: Parcel) : this(parcel.readInt()) {
        deleted = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        type = parcel.readString()
        by = parcel.readString()
        time = parcel.readValue(Long::class.java.classLoader) as? Long
        text = parcel.readString()
        dead = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        parent = parcel.readValue(Int::class.java.classLoader) as? Int
        poll = parcel.readValue(Int::class.java.classLoader) as? Int
        url = parcel.readString()
        score = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        descendants = parcel.readValue(Int::class.java.classLoader) as? Int
        val kidsArray = IntArray(parcel.readInt())
        parcel.readIntArray(kidsArray)
        val partsArray = IntArray(parcel.readInt())
        parcel.readIntArray(partsArray)
        kids = kidsArray.toList()
        parts = kidsArray.toList()
    }

    fun timeSince(): String? {
        if (time == null)
            return null

        // time is unix time (in seconds)
        // joda time works in milliseconds
        val timeMillis = time!! * 1000

        val duration = Interval(timeMillis!!, Instant().millis).toDuration()
        return when {
            duration.standardSeconds < 60 -> pluraliser(duration.standardSeconds, "second", after = " ago")
            duration.standardMinutes < 60 -> pluraliser(duration.standardMinutes, "minute", after = " ago")
            duration.standardHours < 24 -> pluraliser(duration.standardHours, "hour", after = " ago")
            duration.standardDays < 7 -> pluraliser(duration.standardDays, "day", after = " ago")
            else -> DateTimeFormat.forStyle("M-").withLocale(Locale.getDefault()).print(timeMillis!!)
        }
    }

    /**
     * @param number the number that the singularNoun is dependent on
     * @param singularNoun   the singularNoun that can either be singular or plural
     */
    private fun pluraliser(number: Long, singularNoun: String, before: String = "", after: String = "") : String {
        val newNoun = when {
            number <= 1 -> singularNoun
            else -> singularNoun + "s"
        }
        return "$before$number $newNoun$after"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeValue(deleted)
        parcel.writeString(type)
        parcel.writeString(by)
        parcel.writeValue(time)
        parcel.writeString(text)
        parcel.writeValue(dead)
        parcel.writeValue(parent)
        parcel.writeValue(poll)
        parcel.writeString(url)
        parcel.writeValue(score)
        parcel.writeString(title)
        parcel.writeValue(descendants)
        parcel.writeInt(kids.size)
        parcel.writeIntArray(kids.toIntArray())
        parcel.writeInt(parts.size)
        parcel.writeIntArray(parts.toIntArray())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Item> {
        override fun createFromParcel(parcel: Parcel): Item {
            return Item(parcel)
        }

        override fun newArray(size: Int): Array<Item?> {
            return arrayOfNulls(size)
        }
    }
}
