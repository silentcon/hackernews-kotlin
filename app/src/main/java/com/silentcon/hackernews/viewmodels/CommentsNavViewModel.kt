package com.silentcon.hackernews.viewmodels

import android.databinding.BaseObservable
import android.databinding.ObservableField
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.silentcon.hackernews.models.Item

class CommentsNavViewModel(val item: Item, val comments: List<Item>): BaseObservable() {
    var index: Int = 0
    val rootComments
        get() = comments.filter { item -> isRootComment(item) }
    var label: ObservableField<String> = ObservableField("0/0")

    fun next(): Int {
        if (index + 1 < rootComments.size) {
            index++
        }

        updateLabel()
        return comments.indexOf(rootComments[index])
    }

    fun previous(): Int {
        if (index > 0) {
            index--
        }

        updateLabel()
        return comments.indexOf(rootComments[index])
    }

    fun updateLabel() {
        label.set("${index + 1}/${rootComments.size}")
    }

    private fun isRootComment(i: Item) = i.parent == item.id

    val onScrollListener = object: RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            val layoutManager = recyclerView?.layoutManager as LinearLayoutManager
            val commentPos = layoutManager.findFirstCompletelyVisibleItemPosition()
            if (isRootComment(comments[commentPos])) {
                index = rootComments.indexOf(comments[commentPos])
            }
        }
    }
}
