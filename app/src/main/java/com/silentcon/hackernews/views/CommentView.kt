package com.silentcon.hackernews.views

import android.content.Context
import android.content.res.Resources
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.silentcon.hackernews.R
import com.silentcon.hackernews.models.Item
import kotlinx.android.synthetic.main.view_comment.view.*

class CommentView(ctx: Context, attr: AttributeSet) : ConstraintLayout(ctx, attr) {
    var level: Int = 0
        set(value) {
            field = value
            val params = MarginLayoutParams(layoutParams)

            // dp to px
            val density = Resources.getSystem().displayMetrics.density
            val leftMargin = (24 * density * level).toInt()
            params.leftMargin = leftMargin
            layoutParams = params
        }

    var by: String?
        get() = item.by
        set(value) {
            item.by = value
            view_comment_by.text = value
        }

    var time: Long?
        get() = item.time
        set(value) {
            item.time = value
            view_comment_time.text = item.timeSince()
        }

    var text: String?
        get() = item.text
        set(value) {
            item.text = value
            view_comment_text.text = value
        }

    var item = Item()
        set(value) {
            field = value
            by = value.by
            text = value.text
            time = value.time
        }

    init {
        val inflater = LayoutInflater.from(ctx)
        inflater.inflate(R.layout.view_comment, this)

        val array = ctx.obtainStyledAttributes(attr, R.styleable.CommentView)
        by = array.getString(R.styleable.CommentView_comment_view_by)
        text = array.getString(R.styleable.CommentView_comment_view_text)
        time = array.getString(R.styleable.CommentView_comment_view_time)?.toLong()

        array.recycle()
    }
}