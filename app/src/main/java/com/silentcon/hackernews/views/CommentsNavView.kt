package com.silentcon.hackernews.views

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.silentcon.hackernews.R
import com.silentcon.hackernews.databinding.ViewCommentsNavBinding
import com.silentcon.hackernews.viewmodels.CommentsNavViewModel
import kotlinx.android.synthetic.main.view_comments_nav.view.*

class CommentsNavView @JvmOverloads constructor(ctx: Context, attr: AttributeSet? = null, style: Int = 0)
    : LinearLayout(ctx, attr, style) {
    val binding: ViewCommentsNavBinding
    var recyclerview: RecyclerView? = null

    init {
        val inflater = LayoutInflater.from(ctx)
        binding = ViewCommentsNavBinding.inflate(inflater, this, true)

        view_comments_list_up.setOnClickListener { _: View ->
            val newPos = binding.viewModel?.previous() ?: 0
            (recyclerview?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(newPos, 20)
        }
        view_comments_list_down.setOnClickListener { _: View ->
            val newPos = binding.viewModel?.next()
            recyclerview?.layoutManager?.scrollToPosition(newPos ?: 0)
        }
    }

    fun init(recyclerview: RecyclerView) {
        this.recyclerview = recyclerview
        this.recyclerview?.addOnScrollListener(binding.viewModel?.onScrollListener)
    }
}