package com.silentcon.hackernews.views

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.widget.TextViewCompat
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.silentcon.hackernews.R
import com.silentcon.hackernews.models.Item
import kotlinx.android.synthetic.main.view_item.view.*

class ItemView @JvmOverloads constructor(ctx: Context, attr: AttributeSet? = null, defSyleAttr: Int = 0)
    : ConstraintLayout(ctx, attr, defSyleAttr) {

    var score: Int?
        get() = item.score
        set(value) {
            item.score = value
            view_item_score.text = value.toString()
            // Resize if more more than 3 digits so that it fits
            if (value != null && value > 999) {
                val newSize = 22f
                view_item_score.textSize = newSize
            }
        }

    var title: String?
        get() = item.title
        set(value) {
            item.title = value
            val v = findViewById<AppCompatTextView>(R.id.view_item_title)
            v.text = value
        }

    var by: String? = null
        set(value) {
            item.by = value
            val v = findViewById<AppCompatTextView>(R.id.view_item_by)
            v.text = value
        }

    var url: String? = null
        set(value) {
            item.url = value
            val v = findViewById<AppCompatTextView>(R.id.view_item_url)
            v?.text = item.domain
        }

    var time: Long? = null
        set(value) {
            item.time = value
            val v = findViewById<AppCompatTextView>(R.id.view_item_time)
            v?.text = item.timeSince()
        }

    var comments: Int? = null
        set(value) {
            item.descendants = value
            val v = findViewById<AppCompatTextView>(R.id.view_item_comments)
            v?.text = "$value comments"
        }

    var item: Item = Item()
        set(value) {
            field = value
            score = value.score
            title = value.title
            by = value.by
            url = value.url
            time = value.time
            comments = value.descendants
        }

    init {
        val array = ctx.obtainStyledAttributes(attr, R.styleable.ItemView)
        LayoutInflater.from(ctx).inflate(R.layout.view_item, this)

        score = array.getInt(R.styleable.ItemView_item_view_score, 0)
        title = array.getString(R.styleable.ItemView_item_view_title)
        by = array.getString(R.styleable.ItemView_item_view_by)
        url = array.getString(R.styleable.ItemView_item_view_url)
        val t = array.getString(R.styleable.ItemView_item_view_time) ?: ""
        time = t.toLongOrNull()
        comments = array.getInt(R.styleable.ItemView_item_view_comments, 0)

        array.recycle()
    }
}